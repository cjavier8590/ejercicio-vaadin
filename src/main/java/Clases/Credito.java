package Clases;

import java.util.Date;
import java.util.List;

/**
 * Created by Javier Lopez on 3/07/2017.
 */
public class Credito {

    public String monto;
    public double interes;
    public Date fechaInicio;
    public Date fechaFin;

    public List<Pago> pagos;

    public Credito() {
    }

    public Credito(String monto, double interes, Date fechaInicio, Date fechaFin) {
        this.monto = monto;
        this.interes = interes;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(Pago pagos) {
        this.pagos.add(pagos);
    }
}
