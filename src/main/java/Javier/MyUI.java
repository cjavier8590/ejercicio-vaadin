package Javier;

import javax.servlet.annotation.WebServlet;
import javax.swing.*;

import Clases.Cliente;
import Clases.Credito;
import Clases.DatosAdicionales;
import com.sun.javafx.scene.layout.region.Margins;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import javafx.scene.control.SplitPane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        HorizontalSplitPanel ventana = new HorizontalSplitPanel();
        setContent(ventana);



        final VerticalLayout layout = new VerticalLayout();

        final HorizontalLayout div = new HorizontalLayout();
        HorizontalLayout secdiv = new HorizontalLayout();
        HorizontalLayout terdiv = new HorizontalLayout();

        List<Cliente> arreglo = new ArrayList<>();


        final TextField name = new TextField();
        name.setCaption("Primer Nombre");

        final TextField sNombre = new TextField();
        sNombre.setCaption("Segundo Nombre");

        final TextField pApellido = new TextField();
        pApellido.setCaption("Primer Apellido");

        final TextField sApellido = new TextField();
        sApellido.setCaption("Segundo Apellido");
        sApellido.setRequiredIndicatorVisible(true);

        final TextField dpi = new TextField();
        dpi.setCaption("DPI");
        dpi.setRequiredIndicatorVisible(true);


        DateField nacimiento = new DateField();
        nacimiento.setCaption("Fecha de Nacimiento");

        Grid<Cliente> cliente = new Grid<>();

        cliente.addColumn(Cliente::getPrimerNombre).setCaption("Primer Nombre");
        cliente.addColumn(Cliente::getSegundoNombre).setCaption("Segundo Nombre");
        cliente.addColumn(Cliente::getPrimerApellido).setCaption("Primer Apellido");
        cliente.addColumn(Cliente::getSegundoApellido).setCaption("Segundo Apellido");
        cliente.addColumn(Cliente::getFechaNacimiento).setCaption("Fecha de Nacimiento");
        cliente.addColumn(Cliente::getDpi).setCaption("DPI");
        cliente.setWidth("600px");

        Button datos = new Button("Datos Adicionales");
        datos.addClickListener((Button.ClickEvent e) ->{
            //getUI().getNavigator().navigateTo(datosAdicionales.NAME);//

        });

        Button button = new Button("Agregar");
        button.addClickListener( (Button.ClickEvent e) -> {
            Cliente objeto = new Cliente();
            objeto.setPrimerNombre(name.getValue());
            objeto.setSegundoNombre(sNombre.getValue());
            objeto.setPrimerApellido(pApellido.getValue());
            objeto.setSegundoApellido(sApellido.getValue());
            objeto.setDpi(dpi.getValue());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            arreglo.add(objeto);

            for(int x = 0; x <= arreglo.size() - 1; x++){
                cliente.setItems(arreglo.get(x));

            }



        });

        //div.addComponents(name, button);


        secdiv.addComponents(name, sNombre, pApellido);

        terdiv.addComponents( sApellido, dpi, nacimiento);
        layout.addComponents(button,secdiv, terdiv,cliente);
        ventana.setFirstComponent(layout);

        VerticalLayout derecha = new VerticalLayout();
        HorizontalLayout dPrimero = new HorizontalLayout();
        HorizontalLayout dSegundo = new HorizontalLayout();
        TextField direccion = new TextField();
        direccion.setCaption("Direccion");

        TextField telefono = new TextField();
        telefono.setCaption("Telefono");

        TextField estado = new TextField();
        estado.setCaption("Estado");

        TextField nacionalidad = new TextField();
        nacionalidad.setCaption("Nacionalidad");

        Button guarda_adicionales = new Button("Guardar Datos");
        guarda_adicionales.addClickListener((Button.ClickEvent e)  -> {
            DatosAdicionales adicionales = new DatosAdicionales(direccion.getValue(), telefono.getValue(),
                    estado.getValue(), nacionalidad.getValue());
            arreglo.get(0).setDatos(adicionales);
            Notification noti = new Notification("Datos Guardados exitosamente");

        });

        dPrimero.addComponents(direccion, telefono);
        dSegundo.addComponents(estado, nacionalidad, guarda_adicionales);
        derecha.addComponents(dPrimero, dSegundo);

        HorizontalLayout dTercero = new HorizontalLayout();
        HorizontalLayout dCuarto = new HorizontalLayout();

        TextField monto = new TextField();
        monto.setCaption("Monto");

        TextField interes = new TextField();
        interes.setCaption("Interés");

        DateField fechaInicio = new DateField();
        fechaInicio.setCaption("Fecha Inicio");

        DateField fechaFin = new DateField();
        fechaFin.setCaption("Fecha Fin");

        dTercero.addComponents(monto, interes);
        dCuarto.addComponents(fechaInicio, fechaFin);

        Button guarda_credito = new Button("Guardar Crédito");
        guarda_credito.addClickListener((Button.ClickEvent e)  -> {
            Credito credit = new Credito(monto.getValue(), Double.parseDouble(interes.getValue()),
                    null
                    , null);
            arreglo.get(0).setCreditos(credit);
            Notification noti = new Notification("Datos Guardados exitosamente");

        });

        dCuarto.addComponent(guarda_credito);
//final de aplicacion
        derecha.addComponents(dTercero,dCuarto);
        ventana.setSecondComponent(derecha);






    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
